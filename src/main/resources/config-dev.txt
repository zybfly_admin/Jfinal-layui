#主要配置
dbType=mysql
jdbcUrl=jdbc:mysql://localhost/jfinal-layui?characterEncoding=utf8&useSSL=false&zeroDateTimeBehavior=convertToNull
user=root
password=root

#开启jfinal开发模式，便于开发调试
devMode=true
engineDevMode=true

#ureport2报表插件
startUreport2=true

#文件在线预览服务：true-开启，false-关闭,true需要另外启动文件服务器，详情请看项目中的./doc/jfinal-layui启动必读.txt
onlinePreview=false
#文件在线服务地址
onlinePreviewUrl=http://localhost:8012/onlinePreview

#业务配置
projectName=JFinal极速开发企业应用管理系统
baseUploadPath=WEB-INF/temp/upload
baseDownloadPath=WEB-INF/temp/download
copyright=琴海森林(中国)科技有限公司


#oracle数据源配置,多数据源模式
oracle.dbType=oracle
oracle.jdbcUrl=jdbc\:oracle\:thin\:@192.168.0.79\:1521\:orcl
oracle.user=JFinal
oracle.password=JFinal

